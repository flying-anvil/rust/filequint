# Filequint

Delving into the quintessence of files,
`filequint` is a tool for parsing various types of files.
The focus is on more exotic file types.

---

This tool offers two "modes":
- "simple" for gathering less technical information
- "full"/"all", which gathers all possible/supported information

---

This is a Rust port of [the PHP-version (`fileinfo`)](https://gitlab.com/flying-anvil/fileinfo),
which improves performance and is easier to distribute.

---

##  Supported file types

| Type | Extension | Description |
| ---- | --------- | ----------- |
|      |           |             |
